import os
import random
import unicodedata
from itertools import combinations

from gensim.models import KeyedVectors

def get_words(input_filepath: str = os.path.join("resources", "word.txt")) -> dict:
    with open(input_filepath, encoding='utf-8') as file:
        words = unicodedata.normalize("NFKD", file.readline()).encode("ascii", "ignore").decode("utf-8").split(',')
    random.shuffle(words)
    deck = words[:25]
    deck = list(map(lambda elem: elem.lower(), deck))
    red_words = deck[:9]
    blue_words = deck[9:17]
    white_words = deck[17:24]
    black_words = deck[24:25]
    return {'red': red_words, 'blue':blue_words, 'white':white_words, 'black':black_words}

def get_most_valuable_solution(words_wanted: list) -> dict:
    score =[]
    if len(words_wanted) == 1:
        word_solution = model.most_similar(positive=words_wanted[0], topn=1)[0]
        return {'similarity': 1,'words_wanted':[words_wanted[0],words_wanted[0]], 'word_solution':word_solution[0], 'probability':word_solution[1]}
    else:
        for i in range(len(words_wanted)):
            for j in range(i,len(words_wanted)):
                if i!=j:
                    similarite = model.similarity(words_wanted[i], words_wanted[j])
                    word_solution = model.most_similar(positive=[words_wanted[i],words_wanted[j]], topn=1)[0]
                    score.append({'similarity': similarite,'words_wanted':[words_wanted[i],words_wanted[j]], 'word_solution':word_solution[0], 'probability':word_solution[1]})
        return max(score, key=lambda x: x['similarity'])

def play_turn(side) -> None :
    current_side = side
    if side == 'red':
        opposit_side = 'blue'
    elif side =='blue':
        opposit_side = 'red'
    else:
        raise Exception('You have to chose red ou blue side')
    
    print('')
    print('---------------------')
    print(f'|    {current_side.capitalize()} turn !    |')
    print('---------------------')

    sorted_scores = get_most_valuable_solution(words.get(current_side))
    print(f"{current_side.capitalize()}\'s spy gave the word {sorted_scores.get('word_solution')} !")

    if random.random() < sorted_scores.get('probability'):
        print(f"{current_side.capitalize()} team discovered words : {sorted_scores.get('words_wanted')} with the word {sorted_scores.get('word_solution')}.")
        for word in sorted_scores.get('words_wanted'):
            del words[current_side][words[current_side].index(word)]
    else:
        values = [value for key, liste in words.items() if key != current_side for value in liste]
        if values[0] in words[opposit_side]:
            print(f"{current_side.capitalize()} team guesses wrong with word {sorted_scores.get('word_solution')} removing the word {words[opposit_side][words[opposit_side].index(values[0])]} wich was {opposit_side}.")
            del words[opposit_side][words[opposit_side].index(values[0])]
        elif values[0] in words['black']:
            print(f'{current_side.capitalize()} team guesses wrong with word {sorted_scores.get("word_solution")} removing the word {words["black"][words["black"].index(values[0])]} wich was {"black"}.')
            print(f'L\'équipe  {current_side} avait pour indice {sorted_scores.get("word_solution")} et s\'ai trompée, supprimant le mot {words["black"][words["black"].index(values[0])]}')
            del words['black'][words['black'].index(values[0])]
        elif values[0] in words['white']:
            print(f'{current_side.capitalize()} team guesses wrong with word {sorted_scores.get("word_solution")} removing the word {words["white"][words["white"].index(values[0])]} wich was {"white"}.')
            print(f'L\'équipe  {current_side} avait pour indice {sorted_scores.get("word_solution")} et s\'ai trompée, supprimant le mot {words["white"][words["white"].index(values[0])]}')
            del words['white'][words['white'].index(values[0])]



if __name__ == "__main__":
    model = KeyedVectors.load_word2vec_format(os.path.join("resources","frWac_no_postag_no_phrase_500_cbow_cut100.bin"), binary=True, unicode_errors="ignore")

    words: dict = get_words()

    turn = 'red' if len(words.get('red')) > len(words.get('blue')) else 'blue'
    winner = None

    while len(words.get('red')) != 0 and len(words.get('blue'))!=0 and len(words.get('black'))!=0:
        if turn == 'red':
            play_turn('red')
            turn = 'blue'
        elif turn == 'blue':
            play_turn('blue')
            turn = 'red'

        # Check winner
        if len(words['black']) == 0 or len(words['blue']) == 0:
                winner = 'blue'
        elif len(words['red']) == 0:
            winner = 'red'

print(f"The winner is : {winner}")