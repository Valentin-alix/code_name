gensim==4.3.1
numpy==1.24.2
pip==22.3.1
scipy==1.10.1
setuptools==65.5.0
smart-open==6.3.0
